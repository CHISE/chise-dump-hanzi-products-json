# CHISE dump Hanzi-Products JSON
A JSON dump of the ideographic-products feature of the CHISE character ontology.

The ideographic-products [Hanzi-Products] feature represents the
correspondence between each component and the set of its products
(Chinese characters that contain the component).
